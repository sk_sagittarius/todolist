﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToDoList.Models;

namespace ToDoList.Controllers
{
    public class NoteController : Controller
    {
        static List<Note> noteList = new List<Note>
            {
                new Note
                {
                    Id = 1,
                    Name = "first",
                    Text = "Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана. Маленький ручеек Даль журчит по всей стране и обеспечивает ее всеми необходимыми правилами.",
                    Done = false,
                    Tag = TagEnum.Shop.ToString()
                },
                new Note
                {
                    Id = 2,
                    Name = "second",
                    Text = "example",
                    Done = true,
                    Tag = TagEnum.Bussines.ToString()
                }
            };
        // GET: Note
        public ActionResult Index()
        {
            
            return View(noteList);
        }

        [HttpPost]
        public ActionResult Create(Note note)
        {
            var maxId = noteList.Max(x => x.Id);
            note.Id = maxId + 1;
            note.Tag = note.TagEnum.ToString();
            noteList.Add(note);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Search(string name)
        {
            IList<Note> filter = noteList.Where(x => x.Name == name).ToList();
            if (!noteList.Any(x => x.Name == name))
            { return RedirectToAction("Index"); }
            return View("Index", filter);
        }
    }
}