﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ToDoList.Models;

namespace ToDoList.Controllers
{
    public class HomeController : Controller
    {
        Context context = new Context();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Note note)
        {
            context.Notes.Add(note);
            context.SaveChanges();
            return View();
        }

        public ActionResult Details()
        {
            return View();
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Note note = context.Notes.Find(id);
            if (note == null)
            {
                return HttpNotFound();
            }
            return HttpNotFound();
            //Patient = new PatientViewModel
            //{
            //    Id = patient.Id,
            //    Name = patient.Name,
            //    Iin = patient.Iin
            //};

            //var noteList = new NoteList<Note>
            //{
            //    Notes = new Note
            //    {
            //        Id = note.Id,
            //        Name = note.Name,
            //        Text = note.Text,
            //        TagEnum = note.TagEnum,
            //        Done = note.Done
            //    }
            //};
            
            
            //ViewBag.Journals = db.Journals.Where(j => j.Patient.Id == id).ToList();
            //return View(jvm);
        }

    }
}