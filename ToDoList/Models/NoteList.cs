﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoList.Models
{
    public class NoteList
    {
        public List<Note> Notes { get; set; }
    }
}