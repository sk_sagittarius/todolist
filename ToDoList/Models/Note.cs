﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoList.Models
{
    public class Note
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public bool Done { get; set; }
        public string Tag { get; set; }
        public TagEnum TagEnum { get; set; }
    }
}