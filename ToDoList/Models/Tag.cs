﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoList.Models
{
    public class Tag
    {
        public string Text { get; set; }
        public virtual Note Note { get; set; }
    }
}